param(
    [string]$oldName,
    [string]$directory = "./",
    [string]$newName = 's' + $( Get-Random -Maximum 111111111 -Minimum 1111111 ),
    [hashtable]$otherKeywords = @{"MPlanner" = "AssetDynamics" },
    [string]$ignoreFile = "*\**\node_modules"
)
###
# Sample command line 
###
# .\rename-solution.ps1 -oldName "AsnaD.MPlanner" -newName "Dci.Kwl.Kopp" -directory "D:\projects\dci\kwl\kopp" -otherKeywords @{MPlanner="Kopp"; MPPrincipal="KoppPrincipal"}
###


function replaceTextForFiles {
    param( [string]$old, [string]$new, [array]$files)
    foreach ($x in $files) {


        $text = New-Object System.Text.StringBuilder 
        $t = $text.Append([System.IO.File]::ReadAllText($x.FullName));
        $t = $text.Replace($($old), $($new)) ;
        $t = $text.Replace($($old.ToLower()), $($new.ToLower()));
        $t = $text.Replace($($old.ToUpper()), $($new.ToUpper()));
        
        Set-Content -path $x.FullName -Value $t.ToString();
        #[System.IO.File]::WriteAllText($x, ($text.ToString()));
    
    }
}

$fileMask = "*cs|*sln|*bat|*sh|*csproj|*config|*ps1|*cake|*rdlc|*csd|*xsd|*asax|*cshtml|*sass|*datasource|deploy.json|*json|*ts|*tsx|*js|*j|*code-workspace|*yaml|*yml|*gitignore";
$fileExts = ".cs", ".sln", ".bat", ".sh", ".csproj", ".config", ".ps1", ".cake", ".rdlc", ".csd", ".xsd", ".asax", ".cshtml", ".sass", ".datasource", ".swag", ".json", ".json", ".ts", ".tsx", ".js", ".j", ".code-workspace", ".yaml", ".yml", ".gitignore" ;


#Change file content

$files = Get-ChildItem $directory -Exclude $ignoreFile -Recurse | Where-Object { $_.Extension -in $fileExts } | Select-Object FullName;

replaceTextForFiles -old $oldName -new $newName -files $files
D:\projects\misc\solutionsrenamer\grepWin.exe /searchpath:"$directory" /searchfor:"$($oldName)" /filemaskexclude:"$ignoreFile" /regex:no /replacewith:"$($newName)" /filemask:$fileMask /executereplace /k:no /closedialog /i:yes /size:-1 /u:yes  /content
D:\projects\misc\solutionsrenamer\grepWin.exe /searchpath:"$directory" /searchfor:"$($oldName.ToLower())" /filemaskexclude:"$ignoreFile"  /regex:no /replacewith:"$($newName.ToLower())" /filemask:$fileMask /executereplace /k:no /i:yes /size:-1 /u:yes /content /closedialog
D:\projects\misc\solutionsrenamer\grepWin.exe /searchpath:"$directory" /searchfor:"$($oldName.ToUpper())" /filemaskexclude:"$ignoreFile" /regex:no /replacewith:"$($newName.ToUpper())" /filemask:$fileMask /executereplace /k:no /i:yes /size:-1 /u:yes /content /closedialog

#Then directories
Get-ChildItem $directory -Recurse -Directory | Where-Object { $_.name -clike "*$oldName*" } | Rename-Item   -NewName { $_.name -replace "$oldName", "$newName" } -Force
Get-ChildItem $directory -Recurse -Directory | Where-Object { $_.name -clike "*$($oldName.ToLower())*" } | Rename-Item  -NewName { $_.name -replace "$($oldName.ToLower())", "$($newName.ToLower())" } -Force
Get-ChildItem $directory -Recurse -Directory | Where-Object { $_.name -clike "*$($oldName.ToUpper())*" } | Rename-Item  -NewName { $_.name -replace "$($oldName.ToUpper())", "$($newName.ToUpper())" } -Force

#lastly filename
Get-ChildItem $directory -Recurse | Where-Object { $_.name -clike "*$oldName*" } | Rename-Item -NewName { $_.name -replace "$oldName", "$newName" } -Force
Get-ChildItem $directory -Recurse | Where-Object { $_.name -clike "*$($oldName.ToLower())*" } | Rename-Item  -NewName { $_.name -replace "$($oldName.ToLower())", "$($newName.ToLower())" } -Force
Get-ChildItem $directory -Recurse | Where-Object { $_.name -clike "*$($oldName.ToUpper())*" } | Rename-Item  -NewName { $_.name -replace "$($oldName.ToUpper())", "$($newName.ToUpper())" } -Force

#Do the same for each keyword pair entered
foreach ($keyword in $otherKeywords.keys) {
    $files = Get-ChildItem $directory -Exclude $ignoreFile -Recurse | Where-Object { $_.Extension -in $fileExts } | Select-Object FullName ;

    $newWord = $otherKeywords[$keyword];

    replaceTextForFiles -old $keyword -new $newWord -files $files
    D:\projects\misc\solutionsrenamer\grepWin.exe /searchpath:"$directory" /searchfor:"$keyword" /filemaskexclude:"$ignoreFile" /regex:no /replacewith:"$newWord" /filemask:$fileMask /executereplace /k:no /closedialog /i:yes /size:-1 /u:yes  /content
    D:\projects\misc\solutionsrenamer\grepWin.exe /searchpath:"$directory" /searchfor:"$($keyword.ToLower())" /filemaskexclude:"$ignoreFile" /regex:no /replacewith:"$($newWord.ToLower())" /filemask:$fileMask /executereplace /k:no /closedialog /i:yes /size:-1 /u:yes  /content
    D:\projects\misc\solutionsrenamer\grepWin.exe /searchpath:"$directory" /searchfor:"$($keyword.ToUpper())" /filemaskexclude:"$ignoreFile" /regex:no /replacewith:"$($newWord.ToUpper())" /filemask:$fileMask /executereplace /k:no /closedialog /i:yes /size:-1 /u:yes  /content

    Get-ChildItem $directory -Recurse -Directory | Where-Object { $_.name -clike "*$keyword*" } | Rename-Item  -NewName { $_.name -replace "$keyword", "$newWord" } -Force
    Get-ChildItem $directory -Recurse -Directory | Where-Object { $_.name -clike "$($keyword.ToLower())" } | Rename-Item  -NewName { $_.name -replace "$($keyword.ToLower())", "$($newWord.ToLower())" } -Force
    Get-ChildItem $directory -Recurse -Directory | Where-Object { $_.name -clike "$($keyword.ToUpper())" } | Rename-Item  -NewName { $_.name -replace "$($keyword.ToUpper())", "$($newWord.ToUpper())" } -Force

    Get-ChildItem $directory -Recurse | Where-Object { $_.name -clike "*$keyword*" } | Rename-Item  -NewName { $_.name -replace "$keyword", "$newWord" } -Force
    Get-ChildItem $directory -Recurse | Where-Object { $_.name -clike "$($keyword.ToLower())" } | Rename-Item  -NewName { $_.name -replace "$($keyword.ToLower())", "$($newWord.ToLower())" } -Force
    Get-ChildItem $directory -Recurse | Where-Object { $_.name -clike "$($keyword.ToUpper())" } | Rename-Item  -NewName { $_.name -replace "$($keyword.ToUpper())", "$($newWord.ToUpper())" } -Force

}



